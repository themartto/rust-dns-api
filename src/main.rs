#![feature(proc_macro_hygiene, decl_macro)]

mod utils;
mod dns_client;

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate serde_derive;

use rocket_contrib::json::{Json};
use rocket::http::{RawStr, Status};
use rocket::response::status::Custom;
use rocket::response::status;
use std::collections::HashMap;
use trust_dns_client::rr::RecordType;
use rocket::{Request, Response};
use rocket::fairing::{Fairing, Info, Kind};
use rocket::http::Header;

#[derive(Serialize, Deserialize)]
struct QueryResult {
    data: HashMap<String, Vec<String>>
}

#[get("/")]
fn index() -> Custom<String> {
    let message = "I'm a teapot".to_string();
    status::Custom(Status::ImATeapot, message)
}

#[get("/A/<domain>")]
fn a_record(domain: &RawStr) -> Result<Json<QueryResult>, Custom<String>> {
    let domain = match utils::parse_domain(domain) {
        Ok(domain) => domain,
        Err(err) => {
            return Err(status::Custom(
                Status::BadRequest, err,
            ));
        }
    };

    let a_records = match dns_client::lookup_domain_for(
        domain,
        RecordType::A,
    ) {
        Ok(records) => records,
        Err(err) => return Err(
            status::Custom(Status::BadRequest, err)
        )
    };

    Ok(Json(QueryResult { data: a_records }))
}

#[get("/AAAA/<domain>")]
fn aaaa_record(domain: &RawStr) -> Result<Json<QueryResult>, Custom<String>> {
    let domain = match utils::parse_domain(domain) {
        Ok(domain) => domain,
        Err(err) => {
            return Err(status::Custom(
                Status::BadRequest, err,
            ));
        }
    };

    let aaaa_records = match dns_client::lookup_domain_for(
        domain,
        RecordType::AAAA,
    ) {
        Ok(records) => records,
        Err(err) => return Err(
            status::Custom(Status::BadRequest, err)
        )
    };

    Ok(Json(QueryResult { data: aaaa_records }))
}

#[get("/CNAME/<domain>")]
fn cname_record(domain: &RawStr) -> Result<Json<QueryResult>, Custom<String>> {
    let domain = match utils::parse_domain(domain) {
        Ok(domain) => domain,
        Err(err) => {
            return Err(status::Custom(
                Status::BadRequest, err,
            ));
        }
    };

    let cname_records = match dns_client::lookup_domain_for(
        domain,
        RecordType::CNAME,
    ) {
        Ok(records) => records,
        Err(err) => return Err(
            status::Custom(Status::BadRequest, err)
        )
    };

    Ok(Json(QueryResult { data: cname_records }))
}

#[get("/MX/<domain>")]
fn mx_record(domain: &RawStr) -> Result<Json<QueryResult>, Custom<String>> {
    let domain = match utils::parse_domain(domain) {
        Ok(domain) => domain,
        Err(err) => {
            return Err(status::Custom(
                Status::BadRequest, err,
            ));
        }
    };

    let mx_records = match dns_client::lookup_domain_for(
        domain,
        RecordType::MX,
    ) {
        Ok(records) => records,
        Err(err) => return Err(
            status::Custom(Status::BadRequest, err)
        )
    };

    Ok(Json(QueryResult { data: mx_records }))
}

#[get("/NS/<domain>")]
fn ns_record(domain: &RawStr) -> Result<Json<QueryResult>, Custom<String>> {
    let domain = match utils::parse_domain(domain) {
        Ok(domain) => domain,
        Err(err) => {
            return Err(status::Custom(
                Status::BadRequest, err,
            ));
        }
    };

    let ns_records = match dns_client::lookup_domain_for(
        domain,
        RecordType::NS,
    ) {
        Ok(records) => records,
        Err(err) => return Err(
            status::Custom(Status::BadRequest, err)
        )
    };

    Ok(Json(QueryResult { data: ns_records }))
}

#[get("/PTR/<domain>")]
fn ptr_record(domain: &RawStr) -> Result<Json<QueryResult>, Custom<String>> {
    let domain = match utils::parse_domain(domain) {
        Ok(domain) => domain,
        Err(err) => {
            return Err(status::Custom(
                Status::BadRequest, err,
            ));
        }
    };

    let ptr_records = match dns_client::lookup_domain_for(
        domain,
        RecordType::PTR,
    ) {
        Ok(records) => records,
        Err(err) => return Err(
            status::Custom(Status::BadRequest, err)
        )
    };

    Ok(Json(QueryResult { data: ptr_records }))
}

#[get("/SOA/<domain>")]
fn soa_record(domain: &RawStr) -> Result<Json<QueryResult>, Custom<String>> {
    let domain = match utils::parse_domain(domain) {
        Ok(domain) => domain,
        Err(err) => {
            return Err(status::Custom(
                Status::BadRequest, err,
            ));
        }
    };

    let soa_records = match dns_client::lookup_domain_for(
        domain,
        RecordType::SOA,
    ) {
        Ok(records) => records,
        Err(err) => return Err(
            status::Custom(Status::BadRequest, err)
        )
    };

    Ok(Json(QueryResult { data: soa_records }))
}

#[get("/SRV/<domain>")]
fn srv_record(domain: &RawStr) -> Result<Json<QueryResult>, Custom<String>> {
    let domain = match utils::parse_domain(domain) {
        Ok(domain) => domain,
        Err(err) => {
            return Err(status::Custom(
                Status::BadRequest, err,
            ));
        }
    };

    let srv_records = match dns_client::lookup_domain_for(
        domain,
        RecordType::SRV,
    ) {
        Ok(records) => records,
        Err(err) => return Err(
            status::Custom(Status::BadRequest, err)
        )
    };

    Ok(Json(QueryResult { data: srv_records }))
}

#[get("/TXT/<domain>")]
fn txt_record(domain: &RawStr) -> Result<Json<QueryResult>, Custom<String>> {
    let domain = match utils::parse_domain(domain) {
        Ok(domain) => domain,
        Err(err) => {
            return Err(status::Custom(
                Status::BadRequest, err,
            ));
        }
    };

    let txt_records = match dns_client::lookup_domain_for(
        domain,
        RecordType::TXT,
    ) {
        Ok(records) => records,
        Err(err) => return Err(
            status::Custom(Status::BadRequest, err)
        )
    };

    Ok(Json(QueryResult { data: txt_records }))
}

pub struct CORS();

impl Fairing for CORS {
    fn info(&self) -> Info {
        Info {
            name: "Add CORS headers to requests",
            kind: Kind::Response
        }
    }

    fn on_response(&self, _request: &Request, response: &mut Response) {
        response.set_header(Header::new("Access-Control-Allow-Origin", "*"));
        response.set_header(Header::new("Access-Control-Allow-Methods", "GET"));
        response.set_header(Header::new("Access-Control-Allow-Headers", "*"));
        response.set_header(Header::new("Access-Control-Allow-Credentials", "true"));
    }
}

fn rocket() -> rocket::Rocket {
    rocket::ignite()
        .attach(CORS())
        .mount("/",
               routes![index,
               a_record,
               aaaa_record,
               cname_record,
               mx_record,
               ns_record,
               ptr_record,
               soa_record,
               srv_record,
               txt_record],
        )
}

fn main() {
    rocket().launch();
}
