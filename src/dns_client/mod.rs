use addr::DomainName;
use crate::{utils};
use trust_dns_client::rr::{Name, DNSClass, RecordType, Record, RData};
use std::str::FromStr;
use trust_dns_client::op::DnsResponse;
use trust_dns_client::client::{Client, SyncClient};
use std::collections::HashMap;
use trust_dns_client::udp::UdpClientConnection;
use std::net::SocketAddr;

pub fn lookup_domain_for(domain: DomainName, record: RecordType) -> Result<HashMap<String, Vec<String>>, String> {
    let locations = match utils::get_locations() {
        Ok(locations) => locations,
        Err(err) => { return Err(err); }
    };

    let mut result = HashMap::new();

    for (l_name, l_ip) in locations.get_iterator() {
        let ips = match query_for_a(&domain, l_ip, record) {
            Ok(ips) => ips,
            Err(_err) => vec![]
        };
        result.insert(l_name, ips);
    }
    Ok(result)
}

fn query_for_a(domain: &DomainName, ip: String, record_type: RecordType) -> Result<Vec<String>, String> {
    let client = match create_dns_client(ip) {
        Ok(client) => client,
        Err(_err) => { return Err("No DNS server available".to_string()); }
    };


    let name = match Name::from_str(domain.as_str()) {
        Ok(name) => name,
        Err(err) => { return Err(err.to_string()); }
    };

    let response: DnsResponse = match client.query(
        &name,
        DNSClass::IN,
        record_type,
    ) {
        Ok(res) => res,
        Err(err) => { return Err(err.to_string()); }
    };

    let answers: &[Record] = response.answers();

    let mut result = vec![];

    if answers.len() != 0 {
        //answers[0], answers[1] ...
        match record_type {
            RecordType::A => {
                for answer in answers {
                    if let &RData::A(ref ip) = answer.rdata() {
                        result.push(ip.to_string())
                    }
                }
            }
            RecordType::AAAA => {
                for answer in answers {
                    if let &RData::AAAA(ref ip) = answer.rdata() {
                        result.push(ip.to_string())
                    }
                }
            }
            RecordType::CNAME => {
                for answer in answers {
                    if let &RData::CNAME(ref name) = answer.rdata() {
                        result.push(name.to_string())
                    }
                }
            }
            RecordType::MX => {
                for answer in answers {
                    if let &RData::MX(ref name) = answer.rdata() {
                        result.push(format!(
                            "{} {}", name.preference(), name.exchange()
                        ))
                    }
                }
            }
            RecordType::NS => {
                for answer in answers {
                    if let &RData::NS(ref name) = answer.rdata() {
                        result.push(name.to_string())
                    }
                }
            }
            RecordType::PTR => {
                for answer in answers {
                    if let &RData::PTR(ref name) = answer.rdata() {
                        result.push(name.to_string())
                    }
                }
            }
            RecordType::SOA => {
                for answer in answers {
                    if let &RData::SOA(ref name) = answer.rdata() {
                        result.push(format!(
                            "{} {} {} {} {} {} {}",
                            name.mname(), name.rname(), name.serial(),
                            name.refresh(), name.retry(), name.expire(),
                            name.minimum()
                        ))
                    }
                }
            }
            RecordType::SRV => {
                for answer in answers {
                    if let &RData::SRV(ref name) = answer.rdata() {
                        result.push(format!(
                            "{} {} {} {}",
                            name.priority(), name.weight(), name.port(), name.target()
                        ))
                    }
                }
            }
            RecordType::TXT => {
                for answer in answers {
                    if let &RData::TXT(ref name) = answer.rdata() {
                        let txt_val: String = name.txt_data().iter()
                            .map(|s| String::from_utf8((*s).to_vec()).unwrap())
                            .collect::<Vec<String>>()
                            .join("");

                        result.push(txt_val)
                    }
                }
            }
            _ => { return Err("Unknown record type".to_string()); }
        }
    } else {
        return Err("no A records found".to_string());
    }

    if result.len() == 0 {
        return Err("no A records found".to_string());
    }

    Ok(result)
}

fn create_dns_client(ip: String) -> Result<SyncClient<UdpClientConnection>, String> {
    let address: SocketAddr = match ip.parse() {
        Ok(addr) => addr,
        Err(_err) => return Err("Unable to parse the DNS server IP".to_string())
    };

    let conn = match UdpClientConnection::new(address) {
        Ok(conn) => conn,
        Err(err) => return Err(err.to_string())
    };

    // and then create the Client
    Ok(SyncClient::new(conn))
}
