use rocket::http::RawStr;
use addr::DomainName;
use std::fs;
use std::collections::HashMap;

pub fn parse_domain(domain: &RawStr) -> Result<DomainName, String> {
    let valid_domain: DomainName = match domain.parse() {
        Ok(domain) => domain,
        Err(_err) => {
            return Err("Unable to parse domain".to_string());
        }
    };

    return if valid_domain.root().suffix().is_known() {
        Ok(valid_domain)
    } else {
        Err("Invalid domain name provided".to_string())
    };
}

#[derive(Debug, Deserialize)]
pub struct Locations {
    bg: String,
    usa: String,
    de: String,
    nl: String,
    uk: String,
    jp: String,
}

impl Locations {
    pub fn get_iterator(&self) -> HashMap<String, String> {
        let mut locations = HashMap::new();
        locations.insert("BG".to_string(), self.bg.clone());
        locations.insert("USA".to_string(), self.usa.clone());
        locations.insert("DE".to_string(), self.de.clone());
        locations.insert("NL".to_string(), self.nl.clone());
        locations.insert("UK".to_string(), self.uk.clone());
        locations.insert("JP".to_string(), self.jp.clone());

        locations
    }
}

pub fn get_locations() -> Result<Locations, String> {
    let locations_file = match fs::read_to_string("./locations.toml") {
        Ok(file) => file,
        Err(_err) => {
            return Err("Undable to read locations file".to_string());
        }
    };

    let locations: Locations = match toml::from_str(&locations_file) {
        Ok(l) => l,
        Err(_err) => {
            return Err("Unable to parse locations file".to_string());
        }
    };

    Ok(locations)
}
